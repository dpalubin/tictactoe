const game = (function(){
  function createPlayer(marker){
    return {marker, ai:false, wins:0};
  }
  
  const player1 = createPlayer('X');
  const player2 = createPlayer('O');
  let turn = (Math.floor(Math.random()*2)); //A random player goes first
  let currentPlayer = (turn) ? player2 : player1;
  let gameOver = false;
  let ties = 0;

  const gameBoard = (function(){
    /*
    Slots:
      0 | 1 | 2
      3 | 4 | 5
      6 | 7 | 8
  
    Paths to win:
    0    1    2    3    4    5    6    7
    012, 345, 678, 036, 147, 258, 048, 246
    */
    
    let winner = '';
    const paths = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    const slots = [];
    for(let i = 0;i < 9;i++) slots.push('');
    let history = [];

    function _didPlayerWin(marker, testSlots = slots){
      next: for(let i = 0;i < paths.length;i++){
        for(let j = 0;j < 3;j++){
          if(testSlots[paths[i][j]] != marker) continue next;
        }
        return true;  //We only get here if all 3 match marker
      }
      return false;
    }
    function _isTie(testSlots = slots){
      next: for(let i = 0;i < paths.length;i++){
        let m = '';
        for(let j = 0;j < 3;j++){
          if(!m && testSlots[paths[i][j]]){
            m = testSlots[paths[i][j]];
            continue;
          }
          if(m && testSlots[paths[i][j]] && testSlots[paths[i][j]] != m) continue next; //Path is blocked
        }
        return false; //If we get here, this path is unblocked, so game cannot be a tie
      }
      return true;
    }

    function selectSlot(slot, marker){
      if(!slots[slot]){
        slots[slot] = marker;
        if(_didPlayerWin(marker)) winner = marker;
        history.push(slot + marker);
        console.log(history);
        return true;
      }
      return false;
    }
    function clearBoard(){
      for(let i = 0;i < slots.length;i++){
        slots[i] = '';
      }
      winner = '';
      history = [];
      return;
    }
    function isGameOver(){
      if(winner) return true;
      return _isTie();
    }
    function getWinner(){
      return winner;
    }

    function _testOpponent(originalSlots, marker, opponent){
      let testSlots = [...originalSlots];
      let result = 0;
      let tie = false;
      let bestResult = Infinity;
      for(let i = 0; i < testSlots.length; i++){
        if(testSlots[i]) continue;
        testSlots[i] = marker;
        if(_didPlayerWin(marker, testSlots)){
          return 1;
        }
        else if(_isTie(testSlots)){
          tie = true;
        }
        else{
          let opponentResults = _testOpponent(testSlots, opponent, marker);  //Simulate the next turn (opponent) recursively
          //The best score for the current player is the worst score for the opponent
          if(opponentResults < bestResult) bestResult = opponentResults;
        }
        testSlots[i] = '';  //Restore the slot so we can test the next one
      }
      result -= bestResult;
      if(tie && result < 0) result = 0;
      return result;
    }

    function aiSlot(marker, opponent){
      let bestSlot = -1;
      let lowestScore = Infinity;
      for(let i = 0; i < slots.length; i++){
        if(slots[i]) continue;
        slots[i] = marker;
        if(_didPlayerWin(marker)){
          slots[i] = '';
          return i;
        }
        let currentScore = _testOpponent(slots, opponent, marker);  //Run a test to score the opponent's next moves
        if(currentScore < lowestScore || bestSlot < 0){
          lowestScore = currentScore;
          bestSlot = i;
        }
        slots[i] = '';  //Restore the slot so we can keep testing
      }
      return bestSlot;
    }
  
    return {selectSlot, clearBoard, isGameOver, getWinner, aiSlot};
  })();  

  const screenController = (function(){
    const gameDiv = document.querySelector('.game-board');
    const slotButtons = Array.from(document.querySelectorAll('.game-board>button'));
    const replayButton = document.querySelector('.replay-button');
    const player1Wins = document.querySelector('.player1-div .wins');
    const player2Wins = document.querySelector('.player2-div .wins');
    const player1Losses = document.querySelector('.player1-div .losses');
    const player2Losses = document.querySelector('.player2-div .losses');
    const tiesDisplays = Array.from(document.querySelectorAll('.ties'));
    const player1AIToggle = document.querySelector('#player1AI');
    const player2AIToggle = document.querySelector('#player2AI');
    const modal = document.querySelector('.modal');
    const modalContent = document.querySelector('.modal-content');
    const player1Div = document.querySelector('.player1-div');
    const player2Div = document.querySelector('.player2-div');
    
    gameDiv.addEventListener('click',(e) => {
      if(gameOver || e.target.dataset.slot === undefined) return;
      let marker = currentPlayer.marker;  //takeTurn also changes the players turn, so must remember the current marker
      if(takeTurn(e.target.dataset.slot)) e.target.textContent = marker;
    });

    replayButton.addEventListener('click',(e) => {
      gameBoard.clearBoard();
      gameOver = false;
      replayButton.style.display = "none";
      slotButtons.forEach(button => button.textContent = ' ');
      if(currentPlayer.ai) randomTurn();
    });

    player1AIToggle.addEventListener('click',(e) => {
      player1.ai = e.target.checked;
      if(!gameOver && currentPlayer === player1) aiTurn();
    });

    player2AIToggle.addEventListener('click',(e) => {
      player2.ai = e.target.checked;
      if(!gameOver && currentPlayer === player2) aiTurn();
    });

    modal.addEventListener('click',(e) => {
      modal.style.display = 'none';
    });

    function updateSlot(slot, marker){
      slotButtons[slot].textContent = marker;
    }

    function showReplayButton(){
      replayButton.style.display = "block";
      player1Wins.textContent = player1.wins;
      player2Wins.textContent = player2.wins;
      player1Losses.textContent = player2.wins;
      player2Losses.textContent = player1.wins;
      tiesDisplays.forEach(d => d.textContent = ties);
    }

    function gameOverMessage(message){
      modalContent.textContent = message;
      modal.style.display = 'block';
    }

    function highlightActivePlayer(){
      if(currentPlayer === player1){
        player1Div.classList.add('active');
        player2Div.classList.remove('active');
      }
      else{
        player2Div.classList.add('active');
        player1Div.classList.remove('active');
      }
    }
    player1AIToggle.checked = false;
    player2AIToggle.checked = false;
    return {updateSlot, showReplayButton, gameOverMessage, highlightActivePlayer};
  })();

  function aiTurn(){
    let slot = (currentPlayer == player1) ?
                          gameBoard.aiSlot(player1.marker, player2.marker) :
                          gameBoard.aiSlot(player2.marker, player1.marker) ;
    let marker = currentPlayer.marker;  //Need to remember the marker since the next function changes the current player
    takeTurn(slot);
    screenController.updateSlot(slot, marker);
  }

  function randomTurn(){
    let marker = currentPlayer.marker;
    let slot;
    do{
    slot = Math.floor(Math.random()*9);
    }while(!takeTurn(slot))
    screenController.updateSlot(slot, marker);
  }

  function whoWon(){
    let winner = gameBoard.getWinner();
    if(!winner){
      screenController.gameOverMessage('Tie Game!');
      ties++;
      return;
    }
    if(winner === player1.marker){
      player1.wins++;
      screenController.gameOverMessage('Player 1 Wins!');
    }
    else{
      player2.wins++;
      screenController.gameOverMessage('Player 2 Wins!');
    }
  }

  function takeTurn(slot){
    if(!gameBoard.selectSlot(slot, currentPlayer.marker)) return false;
    if(gameBoard.isGameOver()){
      gameOver = true;
      whoWon();
      screenController.showReplayButton();
      return true;
    }
    turn = !turn;
    currentPlayer = (turn) ? player2 : player1;
    screenController.highlightActivePlayer();
    if(currentPlayer.ai) aiTurn();
    return true;
  }

  if(currentPlayer.ai) randomTurn();
  screenController.highlightActivePlayer();
})();
