# TicTacToe

This project was created as part of the Odin Project (TOP) curriculum. The project page can be found [here](https://www.theodinproject.com/lessons/node-path-javascript-tic-tac-toe).

## License
This project is provided under the MIT License, Copyright 2023 David Palubin
